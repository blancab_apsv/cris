<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Admin</title>
</head>
<body>


<form action="CreateResearcherServlet" method="post">
        <input type="text" name="id" placeholder="User Id">
        <input type="text" name="name" placeholder="Name">
        <input type="text" name="lastname" placeholder="Last name">
        <input type="text" name="email" placeholder="Email">
        <button type="submit">Create researcher</button>
</form>

<tr>
<th>Publication</th>
</tr>

<c:forEach items="${publications}" var="pi">
        <tr>
       		 <td> <a href="PublicationServlet?id=${pi.id}"> ${pi.id}</a></td>
        </tr>
</c:forEach>
</table>
</body>
</html>